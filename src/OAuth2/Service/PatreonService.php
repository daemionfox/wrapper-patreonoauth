<?php /** @noinspection PhpUnused */

namespace RCSI\OAuth2\Service;

use OAuth\Common\Http\Url;
use OAuth\OAuth2\Service\AbstractService;
use OAuth\Common\Token\TokenInterface;
use OAuth\OAuth2\Token\StdOAuth2Token;
use OAuth\Common\Http\Exception\TokenResponseException;

/**
 * Class PatreonService
 * @package RCSI\OAuth2\Service
 */
class PatreonService extends AbstractService
{

    /** API URL
     * @var string
     */
    protected $apiURI = "https://www.patreon.com/oauth2/";

    /** Base Sign on
     * @var string
     */
    protected $baseApiUri = 'https://www.patreon.com/';

    /** Auth Endpoint
     * @var string
     */
    protected $authorizationEndpoint = 'https://www.patreon.com/oauth2/authorize';

    /** Access Token endpoint
     * @var string
     */
    protected $accessTokenEndpoint = 'https://www.patreon.com/api/oauth2/token';

    /** Auth Method
     * @var int
     */
    protected $authorizationMethod = self::AUTHORIZATION_METHOD_QUERY_STRING;

    /**
     * Read/write access to profile info only.
     *
     * Includes SCOPE_USER_EMAIL and SCOPE_USER_FOLLOW.
     */
    const SCOPE_USER = 'user';

    /**
     * Parses the access token response and returns a TokenInterface.
     *
     * @param string $responseBody
     * @return TokenInterface
     * @throws TokenResponseException
     */
    protected function parseAccessTokenResponse($responseBody)
    {
        $data = json_decode($responseBody, TRUE);

        if (NULL === $data || !is_array($data))
        {
            throw new TokenResponseException('Unable to parse response.');
        }
        elseif (isset($data[ 'error' ]))
        {
            throw new TokenResponseException('Error in retrieving token: "' . $data[ 'error' ] . '"');
        }

        $token = new StdOAuth2Token();
        $token->setAccessToken($data[ 'access_token' ]);
        $token->setLifetime($data[ 'expires_in' ]);

        if (isset($data[ 'refresh_token' ]))
        {
            $token->setRefreshToken($data[ 'refresh_token' ]);
            unset($data[ 'refresh_token' ]);
        }

        unset($data[ 'access_token' ]);
        unset($data[ 'expires_in' ]);

        $token->setExtraParams($data);

        return $token;
    }

    /** Overload method for getAuthorizationUri
     *
     * @param array $additionalParameters
     * @return Url
     */
    public function getAuthorizationUri(array $additionalParameters = [])
    {
        $state = uniqid();
        $this->storage->storeAuthorizationState($this->service(), $state);

        return parent::getAuthorizationUri(array_merge([
            'state' => $state
        ], $additionalParameters));
    }

    /**
     * Given a namespaced class name, return the actual classname instead of the full path
     *
     * @return string
     */
    public function getClassName()
    {
        $namespacedPath = get_class($this);
        $explodedPath = explode('\\', $namespacedPath);
        /** @noinspection PhpUnnecessaryLocalVariableInspection */
        $lastPieceOfPath = array_pop($explodedPath);
        return $lastPieceOfPath;
    }

}