<?php


namespace RCSI\Patreon;


use RCSI\Exceptions\PatreonCacheException;
use RCSI\Wrapper\ConfigWrapper;

abstract class Component
{
    protected $url;
    protected $cacheDir;
    protected $cacheFile;
    protected $cacheTimeout = 86400;
    protected $config;
    public function __construct(){
        $this->config = ConfigWrapper::init();
        if (file_exists(__DIR__ . "/../../../../../cache") && is_dir(__DIR__ . "/../../../../../cache")) {
            $this->cacheDir = realpath(__DIR__ . "/../../../../../cache");
        } elseif (file_exists(__DIR__ . "/../../../cache") && is_dir(__DIR__ . "/../../../cache")) {
            $this->cacheDir = realpath(__DIR__ . "/../../../cache");
        } else {
            $this->cacheDir = '/tmp';
        }
    }

    abstract public function run();

    public function getCache()
    {
        if (empty($this->cacheDir) || !is_writable($this->cacheDir)) {
            throw new PatreonCacheException("Could not write to cache dir {$this->cacheDir} or it does not exist");
        }
        if (
            !empty($this->cacheFile) &&
            is_file("{$this->cacheDir}/{$this->cacheFile}") &&
            is_readable("{$this->cacheDir}/{$this->cacheFile}")
        ) {
            $filetime = filemtime("{$this->cacheDir}/{$this->cacheFile}");
            $now = time();
            if (($now - $this->cacheTimeout) >= $filetime) {
                throw new PatreonCacheException("Cache file {$this->cacheFile} is too old");
            }
            $data = json_decode(file_get_contents("{$this->cacheDir}/{$this->cacheFile}"), true);
            return $data;
        }
        throw new PatreonCacheException("Cache file {$this->cacheFile} not found or unreadable");
    }

    protected function saveCache($data)
    {
        file_put_contents("{$this->cacheDir}/{$this->cacheFile}", json_encode($data, JSON_PRETTY_PRINT));
        return $this;
    }

}