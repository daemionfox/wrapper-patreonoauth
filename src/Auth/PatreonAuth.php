<?php
/** @noinspection PhpMissingParamTypeInspection */
/** @noinspection PhpMissingReturnTypeInspection */
/** @noinspection PhpMissingFieldTypeInspection */
/** @noinspection SpellCheckingInspection */
/** @noinspection PhpUndefinedNamespaceInspection */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

/**
 * Created by PhpStorm.
 * User: msowers
 * Date: 2/14/2021
 * Time: 8:33 AM
 */

namespace RCSI\Auth;

use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use OAuth\Common\Exception\Exception;
use Patreon\API;
use RCSI\Config\Config;
use RCSI\Exceptions\ConfigException;
use RCSI\Exceptions\PatreonDataException;
use RCSI\Patreon\Tiers;
use RCSI\Provider\PatreonV2;
use RCSI\Wrapper\ConfigWrapper;

use League\Url\Url;
use OAuth\Common\Storage\Exception\TokenNotFoundException;
use OAuth\Common\Token\Exception\ExpiredTokenException;
use OAuth\ServiceFactory;
use OAuth\Common\Consumer\Credentials;
use OAuth\Common\Storage\Session;

/**
 * Class PatreonAuth
 * @package RCSI\Auth
 */
class PatreonAuth
{
    protected static $instance;

    protected $config;

    protected $clientID;
    protected $clientSecret;
    protected $redirectURI;
    protected $creatorAccessToken;

    protected $isLoggedIn = false;
    protected $userDetails;
    protected $token;
    protected $campaign;
    protected $pledgeLevel = 0;
    protected $tierName;
    protected $isPatreon;

    protected $loginTrigger = 'go';
    protected $logoutTrigger = 'logout';

    /**
     * @param $config
     * @return PatreonAuth
     */
    public static function init($config = null): PatreonAuth
    {
        if (empty(self::$instance)) {
            $me = new self();
            if (!empty($config)){
                $me->config = $config;
                $me->clientID = $me->config->get('oauthClientID');
                $me->clientSecret = $me->config->get('oauthSecretKey');
                $me->redirectURI = $me->config->get('oauthRedirectURI');
                $me->creatorAccessToken = $me->config->get('patreonCreatorToken');
                self::$instance = $me;

            }
        }
        return self::$instance;
    }

    /**
     * PatreonAuth constructor.
     * @throws ConfigException
     */
    public function __construct()
    {
    }

    /**
     * @return $this
     * @throws PatreonDataException
     */
    public function auth():PatreonAuth
    {

        if ($this->isLoggedIn === true) {
            return $this;
        }

        $provider = new PatreonV2([
            'clientId' => $this->clientID,
            'clientSecret' => $this->clientSecret,
            'redirectUri' => $this->redirectURI
        ]);

        $token = isset($_COOKIE['patreonoauthaccesstoken']) ? unserialize($_COOKIE['patreonoauthaccesstoken']) : null;
        if (!empty($token)) {
            /**
             * @var $token AccessToken
             */
            if ($token->hasExpired()) {
                $token = null;
                $this->isLoggedIn = false;
            } else {
                $atoken = $this->token = $token->getToken();
                $ctoken = $this->creatorAccessToken;
                $patreon = new API($atoken);
                $creator = new API($ctoken);

                $this->userDetails = $patreon->get_data('identity?include=memberships&fields'.urlencode('[user]').'=email,first_name,full_name,image_url,last_name,thumb_url,url,vanity,is_email_verified&fields'.urlencode('[member]').'=currently_entitled_amount_cents,lifetime_support_cents,last_charge_status,patron_status,last_charge_date,pledge_relationship_start');
                $this->campaign = $creator->get_data('campaigns?include=tiers&fields%5Btier%5D=title,amount_cents');
                $campaignid = isset($this->campaign['data'][0]['id']) ? $this->campaign['data'][0]['id'] : null;
                $cmember = $creator->get_data("campaigns/{$campaignid}/members?include=currently_entitled_tiers&fields%5Bmember%5D=full_name,is_follower,patron_status&fields%5Btier%5D=amount_cents,description,title");


                $memberships = [];
                if (isset($this->userDetails['data']['relationships']['memberships']['data'])) {
                    foreach ($this->userDetails['data']['relationships']['memberships']['data'] as $m) {
                        $memberships[] = $m['id'];
                    }
                }

                $ctiers = new Tiers();

                $tiers = $ctiers->setCampaign($this->campaign)->run();
                $status = null;
                foreach($cmember['data'] as $mem) {
                    if (in_array($mem['id'], $memberships)) {
                        // This is the one we want.
                        $utiers = [];
                        $status = $mem['attributes']['patron_status'];
                        foreach ($mem['relationships']['currently_entitled_tiers']['data'] as $md) {
                            if (!empty($tiers[$md['id']])) {
                                $utiers[] = $tiers[$md['id']];
                            }
                        }
                    }
                }
                if (!empty($utiers)) {
                    usort($utiers, function($a, $b) {
                        return $a['amount'] > $b['amount'];
                    });
                    $this->pledgeLevel = $utiers[0]['amount'];
                    $this->tierName = $utiers[0]['title'];
                    $this->isPatreon = strtoupper($status) === 'ACTIVE_PATRON' && $this->pledgeLevel > 0;
                } else {
                    $this->pledgeLevel = 0;
                    $this->isPatreon = false;
                }
                $this->isLoggedIn = true;

            }
        }

        if (isset($_GET['go']) && strtoupper($_GET['go']) === strtoupper($this->loginTrigger)) {

            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            $authorizationUrl = $provider->getAuthorizationUrl();

            // Get the state generated for you and store it to the session.
            $_SESSION['oauth2state'] = $provider->getState();

            // Redirect the user to the authorization URL.
            header('Location: ' . $authorizationUrl);
            exit;

        } elseif (isset($_GET['logout']) && strtoupper($_GET['logout']) === strtoupper($this->logoutTrigger)) {

        // Fetch the authorization URL from the provider; this returns the
        // urlAuthorize option and generates and applies any necessary parameters
        // (e.g. state).
            unset($token);
            setcookie('patreonoauthaccesstoken', '', time() - 30*86400);

        // Redirect the user to the authorization URL.
            header('Location: ' . $this->config->get('oauthRedirectURI'));
        exit;

    } elseif(empty($token) && isset($_GET['code'])) {

            try {

                // Try to get an access token using the authorization code grant.
                $token = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

                // We have an access token, which we may use in authenticated
                // requests against the service provider's API.
                setcookie('patreonoauthaccesstoken', serialize($token), time() + 30*86400);

                header('Location: ' . $this->config->get('oauthRedirectURI'));
                exit;
            } catch (IdentityProviderException $e) {

                // Failed to get the access token or user details.
                exit($e->getMessage());

            }

        }
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsLoggedIn()
    {
        return $this->isLoggedIn;
    }

    /**
     * @return mixed
     */
    public function getUserDetails()
    {
        return $this->userDetails;
    }

    /**
     * @return mixed
     */
    public function getPledgeLevel()
    {
        return $this->pledgeLevel;
    }

    /**
     * @return mixed
     */
    public function getIsPatreon()
    {
        return $this->isPatreon;
    }

    /**
     * @return mixed
     */
    public function getTierName()
    {
        return $this->tierName;
    }

    /**
     * @param Config $config
     * @return $this
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @param mixed $clientID
     * @return PatreonAuth
     */
    public function setClientID($clientID): PatreonAuth
    {
        $this->clientID = $clientID;
        return $this;
    }

    /**
     * @param mixed $clientSecret
     * @return PatreonAuth
     */
    public function setClientSecret($clientSecret): PatreonAuth
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    /**
     * @param mixed $redirectURI
     * @return PatreonAuth
     */
    public function setRedirectURI($redirectURI): PatreonAuth
    {
        $this->redirectURI = $redirectURI;
        return $this;
    }

    /**
     * @param mixed $creatorAccessToken
     * @return PatreonAuth
     */
    public function setCreatorAccessToken($creatorAccessToken): PatreonAuth
    {
        $this->creatorAccessToken = $creatorAccessToken;
        return $this;
    }

    /**
     * @param string $trigger
     */
    public function setLoginTrigger($trigger = 'go')
    {
        $this->loginTrigger = $trigger;
    }

    /**
     * @param string $trigger
     * @return $this
     */
    public function setLogoutTrigger($trigger = 'logout')
    {
        $this->logoutTrigger = $trigger;
        return $this;
    }

}